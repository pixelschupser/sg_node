$(document).ready(function() {
  // onload
  if($( "div").hasClass('errorpage')) {
    $( "header").addClass('hidden');
  } 

 if($( "body").is("#home")) {
    $( "i.fa-bell-o").parent('div').parent('div').children('form').addClass('hide');
  } 

  if($( "body").is("#rain")) {
    $( ".sensor_settings .wrapper").removeClass('hide');
  } 
 if($( "div.part").hasClass('detail')) {
    var items = document.getElementsByTagName('dt');
    var element_id = items.length+1;
    $( "#timingId").val(element_id);
 }

  $( ".sublist a").each(function( ) {
      if($( "body").attr('id') ==$(this).attr('class')){
        $( ".sublist a").removeClass('active');
        $(this).addClass('active');
        $( "header .back").addClass('hidden');
      }
  });

if ($( ".toggle-switch #timing").val() =='off') {
  $("#timing").parent('.toggle-switch').removeClass('active');
  $( ".toggle-switch #timing").val('on');
}
else if ($( ".toggle-switch #timing").val() =='on') {
 $("#timing").parent('.toggle-switch').addClass('active');
 $( ".toggle-switch #timing").val('off');
}

if ($(".toggle-switch #auto").val()  =='off') {
  $("#auto").parent('.toggle-switch').removeClass('active');
  $(".toggle-switch #auto").val('on');
}
else if ($( ".toggle-switch #auto").val() =='on') {
  $("#auto").parent('.toggle-switch').addClass('active');
  $(".toggle-switch #auto").val('off');
};

if ($( ".toggle-switch #rain").val() =='off') {
  $("#rain").parent('.toggle-switch').removeClass('active');
  $( ".toggle-switch #rain").val('on');
}
else if ($( ".toggle-switch #rain").val() =='on') {
 $("#rain").parent('.toggle-switch').addClass('active');
 $( ".toggle-switch #rain").val('off');
}
if ($( ".toggle-switch #cam").val() =='off') {
  $("#cam").parent('.toggle-switch').removeClass('active');
  $( ".toggle-switch #cam").val('on');
}
else if ($( ".toggle-switch #cam").val() =='on') {
 $("#cam").parent('.toggle-switch').addClass('active');
 $( ".toggle-switch #cam").val('off');
}
if ($( ".toggle-switch #motion").val() =='off') {
  $("#motion").parent('.toggle-switch').removeClass('active');
  $( ".toggle-switch #motion").val('on');
}
else if ($( ".toggle-switch #motion").val() =='on') {
 $("#motion").parent('.toggle-switch').addClass('active');
 $( ".toggle-switch #motion").val('off');
}
if ($( ".toggle-switch #notification").val() =='off') {
  $("#notification").parent('.toggle-switch').removeClass('active');
  $( ".toggle-switch #notification").val('on');
}
else if ($( ".toggle-switch #notification").val() =='on') {
 $("#notification").parent('.toggle-switch').addClass('active');
 $( ".toggle-switch #notification").val('off');
}
// on-click
// overview-page
$( "#btn-takepic").click(function() {
    if ($( "#btn-takepic input").val() =='off') {
      $( "#btn-takepic input").val('on');
    }
    else {
      $( "#btn-takepic input").val('off');
    }
});

$( "#check-cam").click(function() {
    if ($( "#btn-webcam").hasClass('active')) {
      $( "#btn-webcam").val('on');

    }
    else {
      $( "#btn-webcam").val('off');
    }
});

$( "#check-motion").click(function() {
    if ($( "#btn-sensor").hasClass('active')) { 
      $( "#btn-sensor").val('off');
    }
    else {
      $( "#btn-sensor").val('on');
    }
  });


  //toggle
  $( ".toggle").click(function() {
    if ($(this).hasClass('active') ) { 
      $(this).removeClass('active');
    }
    else {
       $( ".toggle").removeClass('active');
      $(this).addClass('active');
    }
  });

  //expand modul
  $( ".part .expand").click(function() {
    if ($(this).hasClass('open')) { 
        $(this).removeClass('open');
        $(this).parent().children('form').addClass('hide');
        $(this).children("h1 + .fa").removeClass('fa-angle-up');
        $(this).children("h1 + .fa").addClass('fa-angle-down');
     }
     else {
      $(this).addClass('open');
      $(this).parent().children('form').removeClass('hide');
      $(this).children("h1 + .fa").removeClass('fa-angle-down');
      $(this).children("h1 + .fa").addClass('fa-angle-up');
     }
  });

  //display overlay
  $(".show_overlay").click(function(e) {
    e.preventDefault();
    if($("div").hasClass('overlay')){
      $("div.overlay").addClass('show');
    }
  });

  $('form.overlay').on('click', '.set_img', function() { 
    if($(this).hasClass('hide_overlay')){ //hide overlay
      $("div.over").removeClass('show');
      $(".overlay").removeClass('show');
      $(".overlay .detail-view").remove();
    }
    // show delet-option
    else if($(this).parent().parent('.overlay').children('div').hasClass('over')) {
      $(this).parent().parent('.overlay').children('.over').addClass('show');
    }
  });

  $('form.overlay').on('click', '.hide_overlay', function() { 
    if($(this).hasClass('btn')){ //hide overlay
        $("div.over").removeClass('show');
    }
  });

   $(".photo-detail").click(function(e) {
    e.preventDefault();
    var getImg = $(this).children('figure');
    var getId = $(getImg).find('img').attr('alt');
    var mywidth = $(getImg).find('img').width();
    $('.overlay').addClass('show');
    $(getImg).clone().appendTo('.overlay');
    $('.overlay figure').addClass('detail-view').css("max-width",'100%');
    $('.over .edit_img .btn.delete').val(getId);
  });

  $(window).resize(function() {
    var my_res_width = $(".photo-detail.overlay").find('img').width();
    $(".photo-detail.overlay .detail-view").css("max-width",'100%' );
  });
});