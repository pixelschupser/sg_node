var cloudinary = require('cloudinary');
var fs = require('fs');
var del = require('node-delete');
var Sensor = require('pi-pir-sensor');
var v4l2camera = require('v4l2camera');
var cam = new v4l2camera.Camera("/dev/video0");     //path to camera
var gpio = require('rpi-gpio');

var datejs= require('../public/js/date.js'); // format current day/time
var datajson= require('../data/data.json'); // get defined data
var s_element = datajson.sensor_info; // stored sensor information
//def motion detection sensor loop = time to loop in seconds
var sensor = new Sensor({ pin: 7,loop: 15000});
gpio.setup(11, gpio.DIR_IN, gpio.EDGE_BOTH);

function automaticControll(isOn) {
  if (isOn == 'on') {
     gpio.on('change', function(channel, value) {
         console.log('Channel ' + channel + ' value is now ' + value);
      });
  } else  if (isOn =='off') {
    gpio.destroy(function() {console.log('closed pins')})
  }
};

function timebaseControll(isOn) {
    if (isOn == 'on') {
      //60 sec btw 1 min. = 60000
      var checking = setInterval(startTiming, 60000);
    }  else  if (isOn == 'off') {
      clearInterval(checking);
    }
};

function StartStop(topic, isOn) {
  if(topic == 'rain') {
      if ( isOn == 'on') {
        gpio.on('change', function(channel, value) {
           console.log('Channel ' + channel + ' value is now ' + value);
        });
      }
      else if ( isOn == 'off') { gpio.destroy(function() {console.log('closed pins')}) }
  }
  else if (topic == 'cam') {
      if ( isOn == 'on') {console.log('start cam'); cam.start()}
      else if ( isOn == 'off') {console.log('stop cam'); cam.stop()}
  }
  else if (topic == 'motion') {
      if ( isOn == 'on') {
        sensor.start(console.log('sensor on'));
        sensor.on('movement', function () {
                console.log('movement', sensor.movement); 
                takephoto();
        })
      }
      else if ( isOn == 'off') {sensor.stop(console.log('sensor off' ))}
  }
  else if (topic == 'notification') {
      if ( isOn == 'on') {}
      else if ( isOn == 'off') {}
  }
}

var camstate = 'off';
var setCameraState = function(isOn) {
  camstate = isOn ? 'on' : 'off';
  console.log('setCameraState', camstate);
  try {
    if (isOn) {
      cam.start(); 
    } else {
      cam.stop();
    }
  } catch(error) {
    console.log('setCameraState:error', error);
    throw error;
  }
};
 
var takephoto = function (req, res, next) {
    var date = new Date().format("dd-MM-yyyy_hh:mm");
    var imageName = "" +  date ;   //title for taken images  
    var saveImage ="img/" + imageName + ".jpg";
    //cam.configSet({formatName: 'YUYV', width: 352, height: 288});
    console.log('take a photo');
     var camWasOn = camstate == 'on' ? true : false;
     if (!camWasOn) {
      setCameraState(true);
     } 
     cam.capture(function() { 
        saveAsJpg(cam.toRGB(), cam.width, cam.height, saveImage);
        if (!camWasOn) {
          setCameraState(false);
        }
        cloudinary.uploader.upload(saveImage, function(result) {
          console.log('img result');
          del(['img/*'], function (err, paths) {});
    },{public_id: imageName});
     });
};

var saveAsJpg = function (rgb, width, height, filename) {
    var jpegjs = require("jpeg-js");
    var size = width * height;
    var rgba = {data: new Buffer(size * 4), width: width, height: height};
    for (var i = 0; i < size; i++) {
        rgba.data[i * 4 + 0] = rgb[i * 3 + 0];
        rgba.data[i * 4 + 1] = rgb[i * 3 + 1];
        rgba.data[i * 4 + 2] = rgb[i * 3 + 2];
        rgba.data[i * 4 + 3] = 255;
    }
    var jpeg = jpegjs.encode(rgba, 100);
    fs.createWriteStream(filename).end(Buffer(jpeg.data));
}; 

function startTiming () {
  var time = new Date().format("hh:mm"); // current time 
  var day = new Date().day('day');  // current day
  console.log('i', new Date().format("hh:mm:ss"));
  for (x in s_element){  
   for (y in s_element[x].content){ 
    var s_run = s_element[x].content[y].runtime;
    if ( s_run != undefined) {
      for (var i = 0; i < s_run.length; i ++){
        var run_day_start = [s_run[i].day, s_run[i].start].join(" "),
             run_day_end = [s_run[i].day,s_run[i].end].join(" "),
             current_time = [day, time].join(" ");
        if (run_day_start ==  current_time) {
          //start tool
          if (s_element[x].id == 'rain') {/* start wifi-socket*/
                gpio.on('change', function(channel, value) {
                console.log('Channel ' + channel + ' value is now ' + value);
              });
            setState ('rain', 'on');
          }
          else if (s_element[x].id == 'cam') {/* start cam*/
            console.log('turn camera on');
              /*if (cam.configGet().formatName !== "YUYV") {
                  console.log("NOTICE: YUYV camera required");
                  process.exit(1);
              }*/  
            setCameraState(true);
            setState ('cam', 'on');
          }
          else if (s_element[x].id == 'motion') {
            /* start motion detection*/
            sensor.start(console.log('sensor on'));
            sensor.on('movement', function () {
                    console.log('movement', sensor.movement); 
                    takephoto();
            })
            setState ('motion', 'on');
          }  
        }  
        else if (run_day_end ==  current_time) {
          console.log('end time', s_element[x].id);
          console.log('r',run_day_end, 'c', current_time, 't:f',run_day_end ==  current_time );
          //end tool
          if (s_element[x].id == 'rain') {/* stop wifi-socket*/
            gpio.destroy(function() {})
            setState ('rain', 'off');
          }
          else if (s_element[x].id == 'cam') {/* stop cam*/
            console.log('cam off' )
            setCameraState(false);
            setState ('cam', 'off');
          }
          else if (s_element[x].id == 'motion') {/* stop motion detection*/
            sensor.stop();
            setState ('motion', 'off');
          }
        }
      }
    }
  }}
};

function setState (Id, State){
  var sId = Id;
  var senState = State;
  var s = datajson.sensor;
  for (x in s) {
         if (s[x].link == sId) {
            s[x].status = senState; 
            fs.writeFile('data/data.json', JSON.stringify(datajson, null, 4) , (err) => {
              if (err) throw err; 
            });
         }
  }
}

module.exports.automaticControll = automaticControll;
module.exports.timebaseControll = timebaseControll;
module.exports.StartStop = StartStop;
module.exports.setState = setState;

//on server start
for (x in s_element) {
  for (y in s_element[x].content ) {
    var a = s_element[x].content[0].automatic;
    var t = s_element[x].content[0].timing;
      if (a == 'on' ){
        automaticControll(a);
      }
      else if (t == 'on' ){
        timebaseControll(t);
        return false;
      }
  }
};

