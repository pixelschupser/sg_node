var express = require('express');
var exphbs  = require('express-handlebars');
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var path = require('path');
var favicon = require('serve-favicon');
var router = express.Router();

var routes = require('./routes/index');

var app = express();

// define templating stuff
app.engine('handlebars', exphbs({ defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.set('views', __dirname+ '/views');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(cookieParser());
app.use(express.static(path.join(__dirname+ '/public'))); 


app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  console.log('404-err', err);
  err.status = 404;
  next(err);
  //res.render('error', { error_404:true});
});

// error handlers
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.log('env-err', err);
    err.status = (err.status || 500);
    res.status(err.status);
    res.render('error', {
      message: err.message,
      error: err,
      error_500: err.status == 500 ? true : false ,
      error_404: err.status == 404 ? true : false
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
    error_500:true
  });
});

module.exports = app;

