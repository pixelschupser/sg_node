var express = require('express');
var router = express.Router();
var fs = require('fs');
var firebase = require('firebase');
var cloudinary = require('cloudinary');

var ref = new Firebase('http://shining-heat-6268.firebaseio.com/app');
//var devRef = new Firebase('http://shining-heat-6268.firebaseio.com/devices');
var datajson= require('../data/data.json');
var cloud = require('../server/cl_conf'); // get our config file
var timingCon = require('../server/time_controll'); // function for time based configuration  
var i = datajson.sensor_info;

var pagetitle = 'Smartgarden';

//DB-Stuff
cloudinary.config(cloud);
var items; 
cloudinary.api.resources(function(result)  { 
  //get stored items
  items =result.resources;
  return items;
});

router
  .get('/styleguide', function(req, res, next) {
    res.render('index', { styleguide: true, title: pagetitle , language : "de", data: datajson, menu: true, id: 'styleguide'});
});

router.get('/', function(req, res, next) {
  res.render('index', { home: true, title: pagetitle , language : "de", data: datajson, menu: true, id: 'home'});
})
router.post('/', function(req, res, next) {
  var senState= req.body.senState;
  var senId = req.body.senId;
  var s = datajson.sensor;
  for (x in s) {
         if (s[x].link == senId) {
            s[x].status = senState; 
            fs.writeFile('data/data.json', JSON.stringify(datajson, null, 4) , (err) => {
              if (err) throw err; 
            });
            timingCon.StartStop(senId, senState);
         }
  }

  res.render('index', { home: true, title: pagetitle , language : "de", data: datajson, menu: true, id: 'home'});
});


var sensor_content;
var time = false;
var sensor_detail = function(url){
  for(var j=0; j < datajson.sensor_info.length; j++){
      if(datajson.sensor_info[j].id == url) {
        sensor_content = datajson.sensor_info[j];
      }
    }
    // check timesettings
    if ( sensor_content.content[0].runtime != undefined) { time = true; }
    else { time = false; }
    return sensor_content;
};


router
  .get('/detail-*', function(req, res, next) {
    var topic = req.params[0];
    sensor_detail(topic);
     if (topic !='notification') {
      res.render('index',  { detail:true, title: pagetitle , language : "de", data: datajson, sensordata: sensor_content.content[0], runtime: time, menu: true, back: true, id: topic, link: topic });
    }
    else {
      res.render('index',  { detail_not:true, title: pagetitle , language : "de", data: datajson, sensordata: sensor_content.content[0], runtime: time, menu: true, back: true, id: topic, link: topic });
    } 
  })
  .post('/detail-*', function(req, res, next) {
    var topic = req.params[0]
    sensor_detail(topic);
    // set control
    var autoC = req.body.autoControll;
    var timeC = req.body.timeControll;
    // delet img
    var deletTimeSetting = req.body.deleteTime;
    //save data
    var day = req.body.day;
    var start = req.body.start;
    var end = req.body.end;
    var tId =  req.body.timingId;
    
      for (x in i) {
         if (i[x].id == topic && topic != 'notification') {
          var a = i[x].content[0].automatic;
          var t = i[x].content[0].timing;
          // check controll setting
          if (autoC != undefined ){
              i[x].content[0].automatic = autoC;
              fs.writeFile('data/data.json', JSON.stringify(datajson, null, 4) , (err) => {
                if (err) throw err; 
              });
              timingCon.automaticControll(autoC);
          }
          else if (timeC != undefined){
              i[x].content[0].timing = timeC;
              fs.writeFile('data/data.json', JSON.stringify(datajson, null, 4) , (err) => {
                if (err) throw err; 
              }); 
             timingCon.timebaseControll(timeC);
          } 
          else if (deletTimeSetting != undefined){
            var runArray =  i[x].content[0].runtime;
            var j = 0;
            for (y in runArray ) {
              if (runArray[y].id == deletTimeSetting) {
                i[x].content[0].runtime.splice(j,1)
                fs.writeFile('data/data.json', JSON.stringify(datajson,null,4) , (err) => {
                  if (err) throw err;
                });
              }
              j = j+1;
            }   
          }
        //check if data to write/save)
          else if (day != undefined && start != undefined && end != undefined && day != '' && start != '' && end != '' ){
            // push data for writing datas
            i[x].content[0].runtime.push(req.body);
            //console.log('run', i[x].content[0].runtime);
             fs.writeFile('data/data.json', JSON.stringify(datajson, null, 4) , (err) => {
              if (err) throw err;
            });
         }
         res.render('index',  { detail:true, title: pagetitle , language : "de", data: datajson, sensordata: sensor_content.content[0], runtime: time, menu: true,  back: true, id: topic, link: topic });
     }
       else {
          // for notifikation view
         res.render('index',  { detail_not:true, title: pagetitle , language : "de", data: datajson, sensordata: sensor_content.content[0], runtime: time, menu: true,  back: true, id: topic, link: topic });
       }
    }
});

router
  .get('/footage', function(req, res, next) {
    res.render('index',  { footage:true, title: pagetitle , language : "de", data: datajson, menu: true, id: 'footage', savedImg: items});
  }) 
  .post('/footage', function(req, res) {
    var img_id = req.body.deleteImg;
    console.log('footage', img_id);
    cloudinary.api.delete_resources(img_id, function(result){
      cloudinary.api.resources(function(result)  { 
        //get stored items
        items =result.resources;
        res.render('index', {  footage:true, title: pagetitle , language : "de", data: datajson, menu: true,  id: 'footage', savedImg: items}); 
        return items;
      });
    });
});

router
  .get('/logout', function(req, res){
      ref.on("value", function(dev) { 
        var url = dev.val().url;
        res.redirect(url);
    });
});

router
  .get('/tools', function(req, res, next) {
    var  devicesRef = require('../server/firebase').child('devices');
    devicesRef.once("value", function(snapshot){
      res.render('index',  { tools:true, title: pagetitle , language : "de", data: datajson, datatools: snapshot.val(), menu: true,  id: 'tools'});
    });
  });

module.exports = router;


 //find wifi-socket
/*var ip = require('network-tools');
ip.getConnectedClients("192.168.178.0", 90, function(err, clients) {
  for (var macAdd in clients) {
      if ( macAdd== 'ac:cf:23:94:0a:72') { 
        console.log('true');
      };
  };
});*/